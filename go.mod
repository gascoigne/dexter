module gitlab.com/gascoigne/dexter

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/go-cmp v0.5.6
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/gascoigne/pogo v0.1.3
	gotest.tools v2.2.0+incompatible
)
