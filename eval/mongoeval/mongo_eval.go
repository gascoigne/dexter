package mongoeval

import (
	"regexp"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/gascoigne/dexter/expr"
)

type Options struct{}

func BuildQuery(e expr.Expr, opt Options) (bson.M, error) {
	switch e := e.(type) {
	case expr.FieldExpr:
		return buildFieldExpr(e, opt)
	case expr.Disjunction:
		return buildDisjunction(e, opt)
	case expr.Conjunction:
		return buildConjunction(e, opt)
	case expr.Negation:
		return buildNegation(e, opt)

	default:
		panic("unreachable")
	}
}

func buildNegation(e expr.Negation, opt Options) (bson.M, error) {
	panic("unimplemented")
}

func buildConjunction(e expr.Conjunction, opt Options) (bson.M, error) {
	var err error
	terms := make(bson.A, len(e))
	for i, v := range e {
		terms[i], err = BuildQuery(v, opt)
		if err != nil {
			return nil, err
		}
	}
	return bson.M{"$and": terms}, nil
}

func buildDisjunction(e expr.Disjunction, opt Options) (bson.M, error) {
	var err error
	terms := make(bson.A, len(e))
	for i, v := range e {
		terms[i], err = BuildQuery(v, opt)
		if err != nil {
			return nil, err
		}
	}
	return bson.M{"$or": terms}, nil
}

func buildFieldExpr(e expr.FieldExpr, opt Options) (bson.M, error) {
	switch lhs := e.Lhs.(type) {
	case expr.LocatorIdent:
		return buildLocatorIdentFieldExpr(lhs, e, opt)
	case expr.LocatorSetMembers:
		return buildLocatorSetMembersFieldExpr(lhs, e, opt)
	default:
		panic("unreachable")
	}
}

func buildLocatorIdentFieldExpr(lhs expr.LocatorIdent, e expr.FieldExpr, opt Options) (bson.M, error) {
	var inner interface{}
	switch e.Op {
	case expr.FieldOpEq:
		p, err := expectPrimitiveAtom(e.Rhs)
		if err != nil {
			return nil, err
		}
		inner = p

	case expr.FieldOpNe:
		p, err := expectPrimitiveAtom(e.Rhs)
		if err != nil {
			return nil, err
		}
		inner = bson.M{"$ne": p}

	case expr.FieldOpReq:
		re, err := expectRegexpAtom(e.Rhs)
		if err != nil {
			return nil, err
		}

		inner = bson.M{"$regex": primitive.Regex{
			Pattern: (*regexp.Regexp)(re).String(),
			Options: "",
		}}

	case expr.FieldOpRneq:
		re, err := expectRegexpAtom(e.Rhs)
		if err != nil {
			return nil, err
		}

		inner = bson.M{"$not": bson.M{"$regex": primitive.Regex{
			Pattern: (*regexp.Regexp)(re).String(),
			Options: "",
		}}}

	default:
		panic("unreachable")
	}

	return bson.M{string(lhs): inner}, nil
}

func expectPrimitiveAtom(a expr.Atom) (interface{}, error) {
	switch a := a.(type) {
	case expr.AtomInt:
		return int(a), nil

	case expr.AtomString:
		return string(a), nil

	case expr.AtomBool:
		return bool(a), nil

	case expr.AtomNil:
		return nil, nil

	case *expr.AtomRegexp:
		return nil, errors.Errorf("expected primitive value: %v", a)
	}

	panic("unreachable")
}

func expectRegexpAtom(a expr.Atom) (*expr.AtomRegexp, error) {
	v, ok := a.(*expr.AtomRegexp)
	if !ok {
		return nil, errors.Errorf("expected regexp value: %v", a)
	}
	return v, nil
}

func buildLocatorSetMembersFieldExpr(lhs expr.LocatorSetMembers, e expr.FieldExpr, opt Options) (bson.M, error) {
	switch e.Op {
	case expr.FieldOpEq:
		p, err := expectPrimitiveAtom(e.Rhs)
		if err != nil {
			return nil, err
		}

		return bson.M{
			string(lhs.Set): bson.M{"$in": bson.A{p}},
		}, nil

	case expr.FieldOpNe:
		p, err := expectPrimitiveAtom(e.Rhs)
		if err != nil {
			return nil, err
		}

		return bson.M{
			"$not": bson.M{
				string(lhs.Set): bson.M{"$in": bson.A{p}},
			},
		}, nil

	case expr.FieldOpReq:
		re, err := expectRegexpAtom(e.Rhs)
		if err != nil {
			return nil, err
		}

		return bson.M{
			string(lhs.Set): bson.M{"$in": bson.A{primitive.Regex{
				Pattern: (*regexp.Regexp)(re).String(),
				Options: "",
			}}},
		}, nil

	case expr.FieldOpRneq:
		re, err := expectRegexpAtom(e.Rhs)
		if err != nil {
			return nil, err
		}

		return bson.M{
			string(lhs.Set): bson.M{"$nin": bson.A{primitive.Regex{
				Pattern: (*regexp.Regexp)(re).String(),
				Options: "",
			}}},
		}, nil
	}
	panic("unreachable")
}
