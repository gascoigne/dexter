package mongoeval_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/gascoigne/dexter/eval/mongoeval"
	"gitlab.com/gascoigne/dexter/parser"
)

type TestCase struct {
	Expr string
	Bson bson.M
}

var testCases = []TestCase{
	{
		Expr: `name == "foo"`,
		Bson: bson.M{
			"name": "foo",
		},
	},
	{
		Expr: `foo.bar.baz == "foo"`,
		Bson: bson.M{
			"foo.bar.baz": "foo",
		},
	},
	{
		Expr: `name != "foo"`,
		Bson: bson.M{
			"name": bson.M{"$ne": "foo"},
		},
	},
	{
		Expr: `name == "foo" AND ok == false`,
		Bson: bson.M{
			"$and": bson.A{
				bson.M{"name": "foo"},
				bson.M{"ok": false},
			},
		},
	},
	{
		Expr: `name == "bar" OR ok == false`,
		Bson: bson.M{
			"$or": bson.A{
				bson.M{"name": "bar"},
				bson.M{"ok": false},
			},
		},
	},
	{
		Expr: `children[_] == "b"`,
		Bson: bson.M{
			"children": bson.M{"$in": bson.A{"b"}},
		},
	},
	{
		Expr: `children[_] != "b"`,
		Bson: bson.M{
			"$not": bson.M{
				"children": bson.M{"$in": bson.A{"b"}},
			},
		},
	},
	{
		Expr: `children[_] =~ /ba+r/`,
		Bson: bson.M{
			"children": bson.M{"$in": bson.A{primitive.Regex{
				Pattern: "ba+r",
				Options: "",
			}}},
		},
	},
	{
		Expr: `children[_] !~ /ba+r/`,
		Bson: bson.M{
			"children": bson.M{"$nin": bson.A{primitive.Regex{
				Pattern: "ba+r",
				Options: "",
			}}},
		},
	},
	{
		Expr: `foo.bar.baz[_] == "b"`,
		Bson: bson.M{
			"foo.bar.baz": bson.M{"$in": bson.A{"b"}},
		},
	},
	{
		Expr: `name =~ /ba+r/`,
		Bson: bson.M{"name": bson.M{"$regex": primitive.Regex{
			Pattern: "ba+r",
			Options: "",
		}}},
	},
	{
		Expr: `name !~ /ba+r/`,
		Bson: bson.M{"name": bson.M{"$not": bson.M{"$regex": primitive.Regex{
			Pattern: "ba+r",
			Options: "",
		}}}},
	},
	{
		Expr: `name == nil`,
		Bson: bson.M{
			"name": nil,
		},
	},
	{
		Expr: `name != nil`,
		Bson: bson.M{
			"name": bson.M{"$ne": nil},
		},
	},
}

func TestEval(t *testing.T) {
	for _, tc := range testCases {
		t.Run(tc.Expr, func(t *testing.T) {
			expr, err := parser.Parse(tc.Expr)
			assert.NoError(t, err, "unable to parse: %v", tc.Expr)

			opts := mongoeval.Options{}
			actual, err := mongoeval.BuildQuery(expr, opts)
			assert.NoError(t, err, "error while building query: %v", tc.Expr)

			assert.Equal(t, actual, tc.Bson, "unexpected query generated")
		})
	}
}
