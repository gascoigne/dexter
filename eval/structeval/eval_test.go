package structeval_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gascoigne/dexter/eval/structeval"
	"gitlab.com/gascoigne/dexter/parser"
)

type TestCase struct {
	Expr  string
	Data  structeval.Data
	Match bool
}

var testCases = []TestCase{
	{
		Expr: `name == "foo"`,
		Data: structeval.NewMapData().
			PutString("name", "foo"),
		Match: true,
	},
	{
		Expr: `name != "foo"`,
		Data: structeval.NewMapData().
			PutString("name", "foo"),
		Match: false,
	},
	{
		Expr: `name != "foo"`,
		Data: structeval.NewMapData().
			PutString("name", "bar"),
		Match: true,
	},
	{
		Expr: `name == "foo" AND ok == false`,
		Data: structeval.NewMapData().
			PutString("name", "foo").
			PutBool("ok", false),
		Match: true,
	},
	{
		Expr: `name == "bar" AND ok == false`,
		Data: structeval.NewMapData().
			PutString("name", "foo").
			PutBool("ok", false),
		Match: false,
	},
	{
		Expr: `name == "foo" AND ok == true`,
		Data: structeval.NewMapData().
			PutString("name", "foo").
			PutBool("ok", false),
		Match: false,
	},
	{
		Expr: `name == "bar" OR ok == false`,
		Data: structeval.NewMapData().
			PutString("name", "foo").
			PutBool("ok", false),
		Match: true,
	},
	{
		Expr: `children[_] == "b"`,
		Data: structeval.NewMapData().
			PutString("name", "foo").
			PutStringSet("children", "a", "b", "c"),
		Match: true,
	},
	{
		Expr: `children[_] == "d"`,
		Data: structeval.NewMapData().
			PutString("name", "foo").
			PutStringSet("children", "a", "b", "c"),
		Match: false,
	},
	{
		Expr: `name =~ /ba+r/`,
		Data: structeval.NewMapData().
			PutString("name", "baaaar"),
		Match: true,
	},
	{
		Expr: `name =~ /ba+r/`,
		Data: structeval.NewMapData().
			PutString("name", "baaaaz"),
		Match: false,
	},
	{
		Expr: `name !~ /ba+r/`,
		Data: structeval.NewMapData().
			PutString("name", "baaaaz"),
		Match: true,
	},
	{
		Expr: `name == nil`,
		Data: structeval.NewMapData().
			PutString("name", "baaaaz"),
		Match: false,
	},
	{
		Expr: `name != nil`,
		Data: structeval.NewMapData().
			PutString("name", "baaaaz"),
		Match: true,
	},
	{
		Expr: `missing == nil`,
		Data: structeval.NewMapData().
			PutString("name", "baaaaz"),
		Match: true,
	},
	{
		Expr: `missing != nil`,
		Data: structeval.NewMapData().
			PutString("name", "baaaaz"),
		Match: false,
	},
}

func TestEval(t *testing.T) {
	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			expr, err := parser.Parse(tc.Expr)
			assert.NoError(t, err, "unable to parse: %v", tc.Expr)

			actual, err := structeval.Eval(expr, tc.Data)
			assert.NoError(t, err, "error while evaluating: %v", tc.Expr)

			assert.Equal(t, actual, tc.Match, "unexpected match results")
		})
	}
}
