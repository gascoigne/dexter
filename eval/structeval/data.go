package structeval

import "regexp"

type Data interface {
	Field(name string) (FieldValue, bool)
}

type FieldValue interface{ isFieldValue() }

type FieldValueString interface {
	FieldValue
	Match(string) bool
	MatchRegexp(*regexp.Regexp) bool
}

type FieldValueInt interface {
	FieldValue
	Match(int) bool
}

type FieldValueBool interface {
	FieldValue
	Match(bool) bool
}

type Set interface {
	isSet()
}
