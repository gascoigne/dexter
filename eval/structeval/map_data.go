package structeval

import "regexp"

//
type MapData map[string]FieldValue

func NewMapData() MapData {
	return make(MapData)
}

func (d MapData) Field(name string) (FieldValue, bool) {
	v, ok := d[name]
	return v, ok
}

func (d MapData) PutString(name string, v string) MapData {
	d[name] = stringValue(v)
	return d
}

func (d MapData) PutBool(name string, v bool) MapData {
	d[name] = boolValue(v)
	return d
}

func (d MapData) PutInt(name string, v int) MapData {
	d[name] = intValue(v)
	return d
}

func (d MapData) PutStringSet(name string, vs ...string) MapData {
	d[name] = NewMapStringSet(vs...)
	return d
}

func (d MapData) PutBoolSet(name string, vs ...bool) MapData {
	d[name] = NewMapBoolSet(vs...)
	return d
}

func (d MapData) PutIntSet(name string, vs ...int) MapData {
	d[name] = NewMapIntSet(vs...)
	return d
}

//
type stringValue string

func (v stringValue) Match(s string) bool {
	return string(v) == s
}

func (v stringValue) MatchRegexp(r *regexp.Regexp) bool {
	return r.MatchString(string(v))
}

func (stringValue) isFieldValue() {}

//
type intValue int

func (v intValue) Match(s int) bool {
	return int(v) == s
}

func (intValue) isFieldValue() {}

//
type boolValue bool

func (v boolValue) Match(s bool) bool {
	return bool(v) == s
}

func (boolValue) isFieldValue() {}

//
type MapStringSet map[string]*struct{}

func NewMapStringSet(vs ...string) MapStringSet {
	m := make(MapStringSet)
	for _, v := range vs {
		m[v] = nil
	}
	return m
}

func (m MapStringSet) Match(v string) bool {
	_, ok := m[v]
	return ok
}

func (m MapStringSet) MatchRegexp(r *regexp.Regexp) bool {
	for v, _ := range m {
		if r.MatchString(v) {
			return true
		}
	}
	return false
}

func (MapStringSet) isSet() {}

func (MapStringSet) isFieldValue() {}

//
type MapIntSet map[int]*struct{}

func NewMapIntSet(vs ...int) MapIntSet {
	m := make(MapIntSet)
	for _, v := range vs {
		m[v] = nil
	}
	return m
}

func (m MapIntSet) Match(v int) bool {
	_, ok := m[v]
	return ok
}

func (MapIntSet) isSet() {}

func (MapIntSet) isFieldValue() {}

//
type MapBoolSet map[bool]*struct{} // is a set of bools ever useful?

func NewMapBoolSet(vs ...bool) MapBoolSet {
	m := make(MapBoolSet)
	for _, v := range vs {
		m[v] = nil
	}
	return m
}

func (m MapBoolSet) Match(v bool) bool {
	_, ok := m[v]
	return ok
}

func (MapBoolSet) isSet() {}

func (MapBoolSet) isFieldValue() {}
