package structeval

import (
	"fmt"
	"regexp"

	"github.com/pkg/errors"

	"gitlab.com/gascoigne/dexter/expr"
)

func Eval(e expr.Expr, data Data) (bool, error) {
	switch e := e.(type) {
	case expr.FieldExpr:
		return evalFieldExpr(e, data)
	case expr.Disjunction:
		return evalDisjunction(e, data)
	case expr.Conjunction:
		return evalConjunction(e, data)
	case expr.Negation:
		return evalNegation(e, data)

	default:
		panic("unreachable")
	}
}

func evalDisjunction(e expr.Disjunction, data Data) (bool, error) {
	for _, e := range e {
		ok, err := Eval(e, data)
		if err != nil {
			return false, err
		}

		if ok {
			return true, nil
		}
	}
	return false, nil
}

func evalConjunction(e expr.Conjunction, data Data) (bool, error) {
	for _, e := range e {
		ok, err := Eval(e, data)
		if err != nil {
			return false, err
		}

		if !ok {
			return false, nil
		}
	}
	return true, nil
}

func evalNegation(e expr.Negation, data Data) (bool, error) {
	ok, err := Eval(e.X, data)
	if err != nil {
		return false, err
	}
	return !ok, nil
}

type intMatchable interface {
	Match(int) bool
}

type stringMatchable interface {
	Match(string) bool
	MatchRegexp(*regexp.Regexp) bool
}

type boolMatchable interface {
	Match(bool) bool
}

func evalFieldExpr(e expr.FieldExpr, data Data) (bool, error) {
	_, isNilRhs := e.Rhs.(expr.AtomNil)
	if isNilRhs {
		return evalFieldExprNil(e, data)
	}

	value, err := locate(e.Lhs, data)
	if err != nil {
		return false, err
	}

	switch value := value.(type) {
	case FieldValueString:
		return evalFieldExprString(e, value, data)
	case FieldValueInt:
		return evalFieldExprInt(e, value, data)
	case FieldValueBool:
		return evalFieldExprBool(e, value, data)
	}

	panic("unreachable")
}

func evalFieldExprNil(e expr.FieldExpr, data Data) (bool, error) {
	fieldExists := locatorExists(e.Lhs, data)
	switch e.Op {
	case expr.FieldOpEq:
		return !fieldExists, nil

	case expr.FieldOpNe:
		return fieldExists, nil

	case expr.FieldOpReq:
		fallthrough

	case expr.FieldOpRneq:
		return false, fmt.Errorf("invalid rhs for regex comparison: nil")
	}

	panic("unreachable")
}

func evalFieldExprString(e expr.FieldExpr, v FieldValueString, data Data) (bool, error) {
	switch e.Op {
	case expr.FieldOpEq:
		s, err := expectStringAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return v.Match(string(s)), nil

	case expr.FieldOpNe:
		s, err := expectStringAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return !v.Match(string(s)), nil

	case expr.FieldOpReq:
		s, err := expectRegexpAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return v.MatchRegexp((*regexp.Regexp)(s)), nil

	case expr.FieldOpRneq:
		s, err := expectRegexpAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return !v.MatchRegexp((*regexp.Regexp)(s)), nil

	default:
		panic("unimplemented")
	}
}

func evalFieldExprInt(e expr.FieldExpr, v FieldValueInt, data Data) (bool, error) {
	switch e.Op {
	case expr.FieldOpEq:
		i, err := expectIntAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return v.Match(int(i)), nil

	case expr.FieldOpNe:
		i, err := expectIntAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return !v.Match(int(i)), nil

	case expr.FieldOpReq:
		return false, errors.Errorf("cannot perform regexp match on integer in expr: %v", e)

	case expr.FieldOpRneq:
		return false, errors.Errorf("cannot perform regexp match on integer in expr: %v", e)

	default:
		panic("unimplemented")
	}
}

func evalFieldExprBool(e expr.FieldExpr, v FieldValueBool, data Data) (bool, error) {
	switch e.Op {
	case expr.FieldOpEq:
		b, err := expectBoolAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return v.Match(bool(b)), nil

	case expr.FieldOpNe:
		b, err := expectBoolAtom(e.Rhs)
		if err != nil {
			return false, err
		}

		return !v.Match(bool(b)), nil

	case expr.FieldOpReq:
		return false, errors.Errorf("cannot perform regexp match on integer in expr: %v", e)

	case expr.FieldOpRneq:
		return false, errors.Errorf("cannot perform regexp match on integer in expr: %v", e)

	default:
		panic("unimplemented")
	}
}

func expectStringAtom(a expr.Atom) (expr.AtomString, error) {
	v, ok := a.(expr.AtomString)
	if !ok {
		return "", errors.Errorf("expected string value: %v", a)
	}
	return v, nil
}

func expectIntAtom(a expr.Atom) (expr.AtomInt, error) {
	v, ok := a.(expr.AtomInt)
	if !ok {
		return 0, errors.Errorf("expected int value: %v", a)
	}
	return v, nil
}

func expectBoolAtom(a expr.Atom) (expr.AtomBool, error) {
	v, ok := a.(expr.AtomBool)
	if !ok {
		return false, errors.Errorf("expected bool value: %v", a)
	}
	return v, nil
}

func expectRegexpAtom(a expr.Atom) (*expr.AtomRegexp, error) {
	v, ok := a.(*expr.AtomRegexp)
	if !ok {
		return nil, errors.Errorf("expected regexp value: %v", a)
	}
	return v, nil
}

func locate(l expr.Locator, data Data) (FieldValue, error) {
	switch l := l.(type) {
	case expr.LocatorIdent:
		v, ok := data.Field(string(l))
		if !ok {
			return nil, errors.Errorf("missing field: %v", l)
		}

		_, ok = v.(Set)
		if ok {
			return nil, errors.Errorf("value is a set: %v", l)
		}

		return v, nil

	case expr.LocatorSetMembers:
		v, ok := data.Field(string(l.Set))
		if !ok {
			return nil, errors.Errorf("missing field: %v", l)
		}

		_, ok = v.(Set)
		if !ok {
			return nil, errors.Errorf("value is not a set: %v", l.Set)
		}

		return v, nil

	}

	panic("unreachable")
}

func locatorExists(l expr.Locator, data Data) bool {
	switch l := l.(type) {
	case expr.LocatorIdent:
		_, ok := data.Field(string(l))
		return ok

	case expr.LocatorSetMembers:
		_, ok := data.Field(string(l.Set))
		return ok

	}

	panic("unreachable")
}
