package parser

import (
	"regexp"
	"strconv"

	"gitlab.com/gascoigne/dexter/expr"
	"gitlab.com/gascoigne/dexter/grammar"
	"gitlab.com/gascoigne/pogo"
)

type exprBuilder struct{}

func (*exprBuilder) VisitLocator(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Locator {
	if p, ok := grammar.IsLocatorIdent(items[0]); ok {
		return grammar.AcceptLocatorIdent(delegate, p)
	}

	if p, ok := grammar.IsLocatorSetMembers(items[0]); ok {
		return grammar.AcceptLocatorSetMembers(delegate, p)
	}

	panic("unreachable")
}

func (*exprBuilder) VisitLocatorIdent(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Locator {
	return expr.LocatorIdent(items[0].(pogo.Item).Value)
}

func (*exprBuilder) VisitLocatorSetMembers(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Locator {
	set := grammar.AcceptLocatorIdent(delegate, items[0]).(expr.LocatorIdent)
	return expr.LocatorSetMembers{
		Set: set,
	}
}

func (*exprBuilder) VisitAtom(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Atom {
	if p, ok := grammar.IsAtomInt(items[0]); ok {
		return grammar.AcceptAtomInt(delegate, p)
	}

	if p, ok := grammar.IsAtomString(items[0]); ok {
		return grammar.AcceptAtomString(delegate, p)
	}

	if p, ok := grammar.IsAtomBool(items[0]); ok {
		return grammar.AcceptAtomBool(delegate, p)
	}

	if p, ok := grammar.IsAtomRegexp(items[0]); ok {
		return grammar.AcceptAtomRegexp(delegate, p)
	}

	if p, ok := grammar.IsAtomNil(items[0]); ok {
		return grammar.AcceptAtomNil(delegate, p)
	}

	panic("unreachable")
}

func (*exprBuilder) VisitAtomInt(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Atom {
	v, err := strconv.Atoi(items[0].(pogo.Item).Value)
	if err != nil {
		panic(err) // TODO
	}
	return expr.AtomInt(v)
}

func (*exprBuilder) VisitAtomString(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Atom {
	lit := items[0].(pogo.Item).Value
	return expr.AtomString(lit[1 : len(lit)-1])
}

func (*exprBuilder) VisitAtomBool(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Atom {
	v, err := strconv.ParseBool(items[0].(pogo.Item).Value)
	if err != nil {
		panic(err) // TODO
	}
	return expr.AtomBool(v)
}

func (*exprBuilder) VisitAtomNil(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Atom {
	return expr.AtomNil{}
}

func (*exprBuilder) VisitAtomRegexp(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Atom {
	lit := items[0].(pogo.Item).Value
	v, err := regexp.Compile(lit[1 : len(lit)-1])
	if err != nil {
		panic(err) // TODO
	}
	return (*expr.AtomRegexp)(v)
}

func (*exprBuilder) VisitFieldOp(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.FieldOp {
	switch items[0].(pogo.Item).Token {
	case grammar.OP_EQ:
		fallthrough
	case grammar.OP_NEQ:
		fallthrough
	case grammar.OP_REQ:
		fallthrough
	case grammar.OP_RNEQ:
		return expr.FieldOp(items[0].(pogo.Item).Value)

	default:
		panic("unreachable")
	}
}

func (*exprBuilder) VisitFieldExpr(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Expr {
	locator := grammar.AcceptLocator(delegate, items[0])
	op := grammar.AcceptFieldOp(delegate, items[1])
	atom := grammar.AcceptAtom(delegate, items[2])
	return expr.FieldExpr{
		Lhs: locator,
		Op:  op,
		Rhs: atom,
	}
}

func (*exprBuilder) VisitExpr(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Expr {
	return grammar.AcceptDisjunction(delegate, items[0])
}

func (*exprBuilder) VisitDisjunction(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Expr {
	cjs := grammar.AllConjunctions(delegate, items)
	xs := make([]expr.Expr, len(cjs))
	copy(xs, cjs)

	// Flatten nested disjunctions
	newXs := make([]expr.Expr, 0)
	for _, x := range xs {
		if x2, ok := x.(expr.Disjunction); ok {
			newXs = append(newXs, x2...)
		} else {
			newXs = append(newXs, x)
		}
	}
	xs = newXs

	if len(xs) == 1 {
		return xs[0]
	}

	return expr.Disjunction(xs)
}

func (*exprBuilder) VisitConjunction(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Expr {
	ngs := grammar.AllNegations(delegate, items)
	xs := make([]expr.Expr, len(ngs))
	copy(xs, ngs)

	// Flatten nested conjunctions
	newXs := make([]expr.Expr, 0)
	for _, x := range xs {
		if x2, ok := x.(expr.Conjunction); ok {
			newXs = append(newXs, x2...)
		} else {
			newXs = append(newXs, x)
		}
	}
	xs = newXs

	if len(xs) == 1 {
		return xs[0]
	}

	return expr.Conjunction(xs)
}

func (*exprBuilder) VisitNegation(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Expr {
	t := grammar.AcceptInnerExpr(delegate, items[1])
	if items[0] == pogo.NilParsed {
		return t
	}

	// Simplify double negations
	if x, ok := t.(expr.Negation); ok {
		return x.X
	}

	return expr.Negation{
		X: t,
	}
}

func (*exprBuilder) VisitInnerExpr(delegate grammar.DexterVisitor, items []pogo.Parsed) expr.Expr {
	if p, ok := grammar.IsFieldExpr(items[0]); ok {
		return grammar.AcceptFieldExpr(delegate, p)
	}

	return grammar.AcceptDisjunction(delegate, items[1])
}
