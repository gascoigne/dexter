package parser_test

import (
	"regexp"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gotest.tools/assert"

	"gitlab.com/gascoigne/dexter/expr"
	"gitlab.com/gascoigne/dexter/parser"
)

type TestCase struct {
	Input  string
	Output expr.Expr
}

var testCases = []TestCase{
	// Simple Field Exprs
	{
		Input: `foo == "bar"`,
		Output: expr.FieldExpr{
			Lhs: expr.LocatorIdent("foo"),
			Op:  expr.FieldOpEq,
			Rhs: expr.AtomString("bar"),
		},
	},
	{
		Input: `foo == 1`,
		Output: expr.FieldExpr{
			Lhs: expr.LocatorIdent("foo"),
			Op:  expr.FieldOpEq,
			Rhs: expr.AtomInt(1),
		},
	},
	{
		Input: `foo == false`,
		Output: expr.FieldExpr{
			Lhs: expr.LocatorIdent("foo"),
			Op:  expr.FieldOpEq,
			Rhs: expr.AtomBool(false),
		},
	},
	{
		Input: `foo =~ /bar(s?)/`,
		Output: expr.FieldExpr{
			Lhs: expr.LocatorIdent("foo"),
			Op:  expr.FieldOpReq,
			Rhs: (*expr.AtomRegexp)(regexp.MustCompile("bar(s?)")),
		},
	},
	{
		Input: `foo !~ /bar(s?)/`,
		Output: expr.FieldExpr{
			Lhs: expr.LocatorIdent("foo"),
			Op:  expr.FieldOpRneq,
			Rhs: (*expr.AtomRegexp)(regexp.MustCompile("bar(s?)")),
		},
	},
	{
		Input: `foos[_] == "bar"`,
		Output: expr.FieldExpr{
			Lhs: expr.LocatorSetMembers{
				Set: expr.LocatorIdent("foos"),
			},
			Op:  expr.FieldOpEq,
			Rhs: expr.AtomString("bar"),
		},
	},
	// complex exprs
	{
		Input: `foo == 1 AND bar =~ /baz*/`,
		Output: expr.Conjunction{
			expr.FieldExpr{
				Lhs: expr.LocatorIdent("foo"),
				Op:  expr.FieldOpEq,
				Rhs: expr.AtomInt(1),
			},
			expr.FieldExpr{
				Lhs: expr.LocatorIdent("bar"),
				Op:  expr.FieldOpReq,
				Rhs: (*expr.AtomRegexp)(regexp.MustCompile("baz*")),
			},
		},
	},
	{
		Input: `foo == 1 AND (bar =~ /baz*/ OR bar =~ /qu+x/)`,
		Output: expr.Conjunction{
			expr.FieldExpr{
				Lhs: expr.LocatorIdent("foo"),
				Op:  expr.FieldOpEq,
				Rhs: expr.AtomInt(1),
			},
			expr.Disjunction{
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpReq,
					Rhs: (*expr.AtomRegexp)(regexp.MustCompile("baz*")),
				},
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpReq,
					Rhs: (*expr.AtomRegexp)(regexp.MustCompile("qu+x")),
				},
			},
		},
	},
	{
		Input: `(foo == 1 AND baz != 2) AND (bar =~ /baz*/ OR bar =~ /qu+x/)`,
		Output: expr.Conjunction{
			expr.FieldExpr{
				Lhs: expr.LocatorIdent("foo"),
				Op:  expr.FieldOpEq,
				Rhs: expr.AtomInt(1),
			},
			expr.FieldExpr{
				Lhs: expr.LocatorIdent("baz"),
				Op:  expr.FieldOpNe,
				Rhs: expr.AtomInt(2),
			},
			expr.Disjunction{
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpReq,
					Rhs: (*expr.AtomRegexp)(regexp.MustCompile("baz*")),
				},
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpReq,
					Rhs: (*expr.AtomRegexp)(regexp.MustCompile("qu+x")),
				},
			},
		},
	},
	{
		Input: `(foo == 1 OR baz != 2) AND (bar =~ /baz*/ OR bar =~ /qu+x/)`,
		Output: expr.Conjunction{
			expr.Disjunction{
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("foo"),
					Op:  expr.FieldOpEq,
					Rhs: expr.AtomInt(1),
				},
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("baz"),
					Op:  expr.FieldOpNe,
					Rhs: expr.AtomInt(2),
				},
			},
			expr.Disjunction{
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpReq,
					Rhs: (*expr.AtomRegexp)(regexp.MustCompile("baz*")),
				},
				expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpReq,
					Rhs: (*expr.AtomRegexp)(regexp.MustCompile("qu+x")),
				},
			},
		},
	},
	{
		Input: `foo == 1 AND NOT (bar == 2)`,
		Output: expr.Conjunction{
			expr.FieldExpr{
				Lhs: expr.LocatorIdent("foo"),
				Op:  expr.FieldOpEq,
				Rhs: expr.AtomInt(1),
			},
			expr.Negation{
				X: expr.FieldExpr{
					Lhs: expr.LocatorIdent("bar"),
					Op:  expr.FieldOpEq,
					Rhs: expr.AtomInt(2),
				},
			},
		},
	},
}

func TestParser(t *testing.T) {
	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			actual, err := parser.Parse(tc.Input)
			assert.NilError(t, err, "error while parsing: %v", tc.Input)
			assert.DeepEqual(t, actual, tc.Output, cmp.Comparer(atomRegexpComparer))
		})
	}
}

func atomRegexpComparer(a, b *expr.AtomRegexp) bool {
	return (*regexp.Regexp)(a).String() == (*regexp.Regexp)(b).String()
}
