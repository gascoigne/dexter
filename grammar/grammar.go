package grammar

import (
	"gitlab.com/gascoigne/dexter/expr"
	. "gitlab.com/gascoigne/pogo"
)

//go:generate nex -e dexter.nex
//go:generate go run ./cmd/visitor-gen/main.go -generate-visitor -path . -public -pkg grammar

var locator,
	locatorIdent,
	locatorSetMembers,
	atom,
	atomInt,
	atomString,
	atomRegexp,
	atomBool,
	atomNil,
	fieldOp,
	fieldExpr,
	Expr,
	disjunction,
	conjunction,
	negation,
	innerExpr Parser

func init() {
	locator = TypedProdIface("Locator", (*expr.Locator)(nil), Or(&locatorSetMembers, &locatorIdent))

	locatorIdent = TypedProdIface("LocatorIdent", (*expr.Locator)(nil), Tok(SYMBOL))

	locatorSetMembers = TypedProdIface("LocatorSetMembers", (*expr.Locator)(nil), Seq(&locatorIdent, Char('['), Char('_'), Char(']')))

	atom = TypedProdIface("Atom", (*expr.Atom)(nil), Or(&atomInt, &atomString, &atomRegexp, &atomBool, &atomNil))

	atomInt = TypedProdIface("AtomInt", (*expr.Atom)(nil), Tok(INT_LITERAL))

	atomString = TypedProdIface("AtomString", (*expr.Atom)(nil), Tok(STRING_LITERAL))

	atomBool = TypedProdIface("AtomBool", (*expr.Atom)(nil), Tok(BOOL_LITERAL))

	atomNil = TypedProdIface("AtomNil", (*expr.Atom)(nil), Tok(NIL))

	atomRegexp = TypedProdIface("AtomRegexp", (*expr.Atom)(nil), Tok(REGEXP_LITERAL))

	fieldOp = TypedProd("FieldOp", expr.FieldOp(""), Or(Tok(OP_EQ), Tok(OP_NEQ), Tok(OP_REQ), Tok(OP_RNEQ)))

	fieldExpr = TypedProdIface("FieldExpr", (*expr.Expr)(nil), Seq(&locator, &fieldOp, &atom))

	Expr = TypedProdIface("Expr", (*expr.Expr)(nil), Seq(&disjunction, EOF))

	disjunction = TypedProdIface("Disjunction", (*expr.Expr)(nil), Seq(&conjunction, Many(Seq(Tok(KW_OR), &conjunction))))

	conjunction = TypedProdIface("Conjunction", (*expr.Expr)(nil), Seq(&negation, Many(Seq(Tok(KW_AND), &negation))))

	negation = TypedProdIface("Negation", (*expr.Expr)(nil), Seq(Maybe(Tok(KW_NOT)), &innerExpr))

	innerExpr = TypedProdIface("InnerExpr", (*expr.Expr)(nil), Or(&fieldExpr, Seq(Char('('), &disjunction, Char(')'))))
}
