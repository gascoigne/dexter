package main

import (
	"flag"

	_ "gitlab.com/gascoigne/dexter/grammar"
	"gitlab.com/gascoigne/pogo"
)

var (
	path = flag.String("path", "", "")
	pkg  = flag.String("pkg", "", "")
)

func main() {
	flag.Parse()
	pogo.VisitorTemplate.Generate("dexter", *pkg, *path)
}
