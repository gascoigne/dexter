package grammar

import "gitlab.com/gascoigne/pogo"

const (
	INT_LITERAL    = pogo.TokenType("INT_LITERAL")
	STRING_LITERAL = pogo.TokenType("STRING_LITERAL")
	BOOL_LITERAL   = pogo.TokenType("BOOL_LITERAL")
	REGEXP_LITERAL = pogo.TokenType("REGEXP_LITERAL")
	NIL            = pogo.TokenType("NIL")
	SYMBOL         = pogo.TokenType("SYMBOL")
	OP_EQ          = pogo.TokenType("OP_EQ")
	OP_NEQ         = pogo.TokenType("OP_NEQ")
	OP_REQ         = pogo.TokenType("OP_REQ")
	OP_RNEQ        = pogo.TokenType("OP_RNEQ")
	KW_AND         = pogo.TokenType("KW_AND")
	KW_OR          = pogo.TokenType("KW_OR")
	KW_NOT         = pogo.TokenType("KW_NOT")
)
