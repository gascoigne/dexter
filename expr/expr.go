// Package expr contains the internal representation of an expression
package expr

import "regexp"

// A Locator is placed on the left hand side of a field expression. It selects the field(s) to match against.
type Locator interface{ isLocator() }

// LocatorIdent is a simple identifier.
type LocatorIdent string

func (LocatorIdent) isLocator() {}

// LocatorSetMembers selects all members of a set.
type LocatorSetMembers struct {
	Set LocatorIdent
}

func (LocatorSetMembers) isLocator() {}

// An Atom is a value placed on the right hand side of a field expression.
type Atom interface{ isAtom() }

// An AtomInt is an integer value.
type AtomInt int

func (AtomInt) isAtom() {}

// An AtomString is a string value.
type AtomString string

func (AtomString) isAtom() {}

// An AtomRegexp is a regular expression.
type AtomRegexp regexp.Regexp

func (*AtomRegexp) isAtom() {}

// An AtomBool is a boolean value.
type AtomBool bool

func (AtomBool) isAtom() {}

// An AtomNil is a nil value.
type AtomNil struct{}

func (AtomNil) isAtom() {}

// An Expr is an expression.
type Expr interface{ isExpr() }

// A FieldOp is the operator used in a field expression.
type FieldOp string

const (
	FieldOpEq   FieldOp = "=="
	FieldOpNe   FieldOp = "!="
	FieldOpReq  FieldOp = "=~"
	FieldOpRneq FieldOp = "!~"
)

// A FieldExpr is an expression over one or more fields.
type FieldExpr struct {
	Lhs Locator
	Op  FieldOp
	Rhs Atom
}

func (FieldExpr) isExpr() {}

// A Disjunction is a logical or of one or more subexpressions.
type Disjunction []Expr

func (Disjunction) isExpr() {}

// A Conjunction is a logical and of one or more subexpressions.
type Conjunction []Expr

func (Conjunction) isExpr() {}

// A Negation is an logical not of a subexpression.
type Negation struct {
	X Expr
}

func (Negation) isExpr() {}
